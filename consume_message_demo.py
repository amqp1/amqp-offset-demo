"""Demo application for AMQP 1.0 offset consuming"""
from __future__ import print_function
import os
import json
import argparse
import logging

from proton import SSLDomain
from proton.handlers import MessagingHandler
from proton.reactor import Container, ReceiverOption, Filter

# Path to certificates
CERT_PATH: str = "./certs/"
LOG_LEVEL = "INFO"


class Parameters:
    """Read app arguments from command line arguments or alternatively from environment"""

    def __init__(self, args):
        self.host = args.host if args.host else os.getenv("AMQP_HOST_NAME")
        self.port = args.port if args.port else int(os.getenv("AMQP_HOST_PORT"))
        self.user = args.user if args.user else os.getenv("AMQP_USER")
        self.password = args.password if args.password else os.getenv("AMQP_PASSWORD")
        self.model_name = (
            args.model_name if args.model_name else os.getenv("AMQP_DATA_EXCHANGE_NAME")
        )
        # handle possible arguments, init empty when no arguments
        self.arguments = (
            json.loads(args.arguments)
            if args.arguments
            else json.loads(os.getenv("AMQP_ARGUMENTS"))
            if os.getenv("AMQP_ARGUMENTS")
            else {}
        )


class ReceiveHandler(MessagingHandler):
    """Start a consumer and print incoming AMQP 1.0 messages"""

    def __init__(self, conn_url, port, address, user, password):
        super(ReceiveHandler, self).__init__()

        self.conn_url = "amqps://" + conn_url + ":" + str(port)
        self.address = "/amq/queue/" + address
        self.user = user
        self.password = password

    def on_start(self, event):
        ssl_domain: SSLDomain = self._setup_ssl_domain(
            CERT_PATH + "client_certificate.pem", CERT_PATH + "client_key.pem"
        )

        conn = event.container.connect(
            self.conn_url,
            sasl_enabled=True,
            ssl_domain=ssl_domain,
            heartbeat=60,
            virtual_host="",
            allowed_mechs="PLAIN",
            allow_insecure_mechs=False,
            user=self.user,
            password=self.password,
        )
        receiver = event.container.create_receiver(conn, self.address, options=CapabilityOptions())
        # setting offset to first. still in rabbitmq it is handled as "next/last"
        Filter(filter_set={"rabbitmq:stream-offset-spec": "first"}).apply(receiver)

    def _setup_ssl_domain(self, cert_file: str, key_file: str) -> SSLDomain:
        """Setup SSL domain with given certificate files"""
        my_domain = SSLDomain(SSLDomain.MODE_CLIENT)
        my_domain.set_credentials(
            cert_file=cert_file,
            key_file=key_file,
            password=None,
        )
        my_domain.set_peer_authentication(SSLDomain.VERIFY_PEER)
        return my_domain

    def on_link_opened(self, event):
        logging.info(
            f"SEND: Opened receiver for target model_name '{event.receiver.source.address}'"
        )

    def on_message(self, event):
        message = event.message

        logging.info(f"RECEIVE: Received message '{message.body}'")

    # Very basic event handling for test purposes
    def on_disconnected(self, event):
        logging.warning("Disconnected")

    def on_connection_error(self, event):
        logging.error("Connection error")

    def on_link_closed(self, event):
        logging.warning("Link closed")

    def on_session_error(self, event):
        logging.error("Session error")

    def on_transport_error(self, event):
        logging.error("Transport error")


class CapabilityOptions(ReceiverOption):
    """Sets durability"""

    def apply(self, receiver):
        receiver.source.durability = 2  # Deliveries; A terminus with both durably held configuration and durably held delivery state.


def main():
    parser = argparse.ArgumentParser(
        description="""To start consuming messages from a queue, exchange or stream, export build path (adjust to your env):
    export PYTHONPATH=/home/path/to/app/
    Then either:
    1) Type: python3 ./consume_message_demo.py --host <host> --port <port> --user <user> --password <password> --model_name <data-exchange-name> [--arguments <arguments>]
    2) Define following environment variables and type: python3 ./consume_message_demo.py
    """
    )
    parser.add_argument("--host")
    parser.add_argument("--port")
    parser.add_argument("--user")
    parser.add_argument("--password")
    parser.add_argument("--model_name")
    parser.add_argument("--arguments")
    args = parser.parse_args()

    params = Parameters(args)

    logging.info(
        f"Parameters: host={params.host}, port={params.port}, user={params.user}, password=****, model_name={params.model_name}"
    )

    handler = ReceiveHandler(
        params.host,
        params.port,
        params.model_name,
        params.user,
        params.password,
    )
    container = Container(handler)
    container.run()


if __name__ == "__main__":
    try:
        logging.basicConfig(level=LOG_LEVEL)
        main()
    except KeyboardInterrupt:
        pass

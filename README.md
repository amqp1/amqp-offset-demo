# amqp-offset-demot-package

## Purpose

This repo provides a demo AMQP Client implementation with Apache Qpid Proton for offset handling with RabbitMQ.

## Preparation

#### Dependencies
When installing the Qpid Proton (see requirements.txt) please note that the installation on macOS can be tricky as some SSL dependencies are not being installed.
On unix these additional packages for Qpid Proton are required:
gcc swig pkg-config libssl-dev

```
pip3 install -r requirements.txt 
```

#### Certificates
Copy your client certificate and key file into the certs folder. Update the certificate names to match the following naming:
```
client_certificate.pem
client_key.pem
```

#### Pythonpath
Before executing update environment variable PYTHONPATH to project path.

```
export PYTHONPATH=/path/to/app-package/
```

## Run the App

#### Create a TLS AMQP 1.0 connection through CLI arguments

The connection parameters for the AMQP consumer app be set directly via command line arguments.

Start Consumer:
```
python3 ./consume_message_demo.py --host <host> --port <port> --user <user> --password <password> --model_name <data-exchange-name> [--arguments <arguments>] 
```

#### Create a TLS AMQP 1.0 connection with environment variables
Instead of handing the connection parameters through CLI arguments, you can also set them as environment variables (useful for Docker & co).

First set the following environment variables:
```
export PYTHONPATH="${HOME}/path/to/app-package/"
export AMQP_DATA_EXCHANGE_NAME=<stream-name> e.g. "test-stream"
export AMQP_HOST_NAME=<host-name> e.g. "test-broker-rabbitmq.local"
export AMQP_HOST_PORT=<port> e.g. "5671"
export AMQP_USER=<username> e.g. "testuser"
export AMQP_PASSWORD=<password> e.g. "secret-password"
export AMQP_ARGUMENTS=<additional-arguments> e.g. "" (by default can be empty)
```
Start Consumer
```
python3 ./consume_message_demo.py 
```